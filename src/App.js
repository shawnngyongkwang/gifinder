import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Login from "./components/Login";
import SearchPage from "./components/SearchPage";
import FavouritesPage from "./components/FavouritesPage";
import { Spin } from "antd";
import "./App.css";
import "antd/dist/antd.css";
import { CSSTransition } from "react-transition-group";

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isSignedIn: null,
      favourites: [],
    };
  }

  initializeGoogleSignIn() {
    window.gapi.load("auth2", () => {
      window.gapi.auth2
        .init({
          client_id: `${process.env.REACT_APP_GOOGLE_CLIENT_ID}`,
        })
        .then(() => {
          const authInstance = window.gapi.auth2.getAuthInstance();
          const isSignedIn = authInstance.isSignedIn.get();
          this.setState({ isSignedIn });

          authInstance.isSignedIn.listen((isSignedIn) => {
            this.setState({ isSignedIn });
          });
        });
    });
  }

  setFavourites = (newFavourites) => {
    this.setState({ favourites: [...newFavourites] });
  };

  componentDidMount() {
    const script = document.createElement("script");
    script.src = "https://apis.google.com/js/platform.js";
    script.onload = () => this.initializeGoogleSignIn();
    document.body.appendChild(script);
  }

  ifUserSignedIn(Component) {
    if (this.state.isSignedIn === null) {
      return (
        <Spin
          className="page-loading-spinner"
          size="large"
          tip="Checking if you're signed in..."
        />
      );
    }
    return this.state.isSignedIn ? (
      Component
    ) : (
      <CSSTransition
        in={true}
        appear={true}
        timeout={1000}
        classNames="login-page"
      >
        <Login />
      </CSSTransition>
    );
  }

  render() {
    return (
      <Router basename={"/gifinder"}>
        <div className="App">
          <Switch>
            <Route
              exact
              path="/"
              render={() =>
                this.ifUserSignedIn(
                  <SearchPage
                    favourites={this.state.favourites}
                    setFavourites={this.setFavourites}
                  />
                )
              }
            />
            <Route
              path="/favourites"
              render={() =>
                this.ifUserSignedIn(
                  <FavouritesPage
                    favourites={this.state.favourites}
                    setFavourites={this.setFavourites}
                  />
                )
              }
            />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
