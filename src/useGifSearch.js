import { useEffect, useState } from "react";
import axios from "axios";

export default function useGifSearch(query, pageNumber) {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const [gifs, setGifs] = useState([]);
  const [hasMore, setHasMore] = useState(false);
  const LIMIT = 12;

  // For every new query, give a new list of GIFs rather than appending to the back of the current list
  useEffect(() => {
    setGifs([]);
  }, [query]);

  useEffect(() => {
    setLoading(true);
    setError(false);
    let cancel;
    let urlToUse;
    let offset = (pageNumber - 1) * LIMIT;

    if (query === "") {
      // If search bar is empty, populate results with trending GIFs
      urlToUse = `https://api.giphy.com/v1/gifs/trending?api_key=${process.env.REACT_APP_GIPHY_API_KEY}&limit=${LIMIT}&offset=${offset}`;
    } else {
      urlToUse = `https://api.giphy.com/v1/gifs/search?api_key=${
        process.env.REACT_APP_GIPHY_API_KEY
      }&q=${query.replace(/\s/g, "+")}&limit=${LIMIT}&offset=${offset}`;
    }

    axios({
      method: "GET",
      url: urlToUse,
      cancelToken: new axios.CancelToken((c) => (cancel = c)),
    })
      .then((res) => {
        setGifs((prevGifs) => {
          return [...new Set([...prevGifs, ...res.data.data])];
        });
        setHasMore(res.data.data.length > 0);
        setLoading(false);
      })
      .catch((e) => {
        if (axios.isCancel(e)) return;
        setError(true);
      });
    return () => cancel();
  }, [query, pageNumber]);
  return { loading, error, gifs, hasMore };
}
