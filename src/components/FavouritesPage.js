import React, { useState, useEffect } from "react";
import NavBar from "./NavBar";
import GifModal from "./GifModal";
import { Layout } from "antd";
import MediaQuery from "react-responsive";
import FavGifList from "./FavGifList";
import GIFinderLogo from "../assets/GIFinder-logo.png";

export default function FavouritesPage(props) {
  const [selectedGif, setSelectedGif] = useState(null);
  const [modalIsOpen, setModalIsOpen] = useState(false);

  const authInstance = window.gapi.auth2.getAuthInstance();
  const user = authInstance.currentUser.get();
  const profile = user.getBasicProfile();
  const email = profile.getEmail();
  const { setFavourites } = props;

  const { Content, Footer } = Layout;

  const openModal = (gif) => {
    setModalIsOpen(true);
    setSelectedGif(gif);
  };

  const closeModal = () => {
    setModalIsOpen(false);
    setSelectedGif(null);
  };

  const gifItems = props.favourites.map((image) => {
    return [image.id, image, (selectedGif) => openModal(selectedGif)];
  });

  useEffect(() => {
    const getFavourites = JSON.parse(localStorage.getItem(email) || "0");
    if (getFavourites !== 0) {
      setFavourites([...getFavourites]);
    } else {
      setFavourites([]);
    }
  }, [setFavourites, email]);

  return (
    <Layout className="layout">
      <MediaQuery maxWidth={550}>
        <div className="mobile-header-logo-container">
          <img
            className="mobile-header-logo"
            src={GIFinderLogo}
            alt="GIFinder logo"
          />
        </div>
      </MediaQuery>
      <div className="site-layout-header">
        <NavBar clearTerm={() => {}} />
      </div>
      <Content className="site-layout-content">
        <FavGifList
          gifs={gifItems}
          onGifSelect={(selectedGif) => openModal(selectedGif)}
        />
        <GifModal
          modalIsOpen={modalIsOpen}
          selectedGif={selectedGif}
          onRequestClose={() => closeModal()}
          favourites={props.favourites}
          setFavourites={props.setFavourites}
        />
      </Content>
      <MediaQuery minHeight={700}>
        <Footer className="site-layout-footer">
          GIFinder, powered by GIPHY | Created by Shawn Ng | 2021
        </Footer>
      </MediaQuery>
    </Layout>
  );
}
