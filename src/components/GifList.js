import React from "react";
import GifItem from "./GifItem";
import { Grid, AutoSizer } from "react-virtualized";
import { useMediaQuery } from "react-responsive";

const GifList = (props) => {
  const gifItems = props.gifs.map((image, index) => {
    if (props.gifs.length === index + 1) {
      return [true, image.id, image, props.onGifSelect, props.lastGifRef];
    } else {
      return [false, image.id, image, props.onGifSelect];
    }
  });

  // Check width
  const isTinyScreen = useMediaQuery({
    query: "(max-width: 645px)",
  });

  const isMobileScreen = useMediaQuery({
    query: "(max-width: 900px)",
  });

  const isSmallScreen = useMediaQuery({
    query: "(max-width: 1300px)",
  });

  const isMediumScreen = useMediaQuery({
    query: "(max-width: 1700px)",
  });

  // Check height
  const isSmallHeight = useMediaQuery({
    query: "(max-height: 800px)",
  });

  const isMobileLandscape =
    useMediaQuery({
      query: "(max-height: 450px)",
    }) && isMobileScreen;

  const isSmallLandscape =
    isSmallHeight && isMobileScreen && !isMobileLandscape;

  let numCols;

  if (isSmallHeight) {
    numCols = isSmallLandscape
      ? 2
      : isMobileScreen
      ? 3
      : isMobileLandscape
      ? 3
      : isSmallScreen
      ? 3
      : 4;
  } else {
    numCols = isMobileScreen || isSmallScreen ? 2 : isMediumScreen ? 3 : 4;
  }

  const cellRenderer = ({ rowIndex, columnIndex, style }) => {
    const gif = gifItems[rowIndex * numCols + columnIndex];
    if (gif == null) {
      return;
    }
    if (gif[0]) {
      return (
        <div style={style} key={gif[1]}>
          <GifItem gif={gif[2]} onGifSelect={gif[3]} lastGifRef={gif[4]} />
        </div>
      );
    } else {
      return (
        <div style={style} key={gif[1]}>
          <GifItem gif={gif[2]} onGifSelect={gif[3]} />
        </div>
      );
    }
  };

  return (
    <div className="gif-list">
      <AutoSizer>
        {({ width, height }) => {
          return (
            <Grid
              columnCount={numCols}
              columnWidth={Math.ceil(width / numCols)}
              width={width}
              height={height}
              rowHeight={
                isTinyScreen
                  ? 150
                  : isMobileLandscape
                  ? 200
                  : isMobileScreen || isSmallHeight
                  ? 250
                  : 350
              }
              rowCount={Math.ceil(gifItems.length / numCols)}
              cellRenderer={cellRenderer}
            />
          );
        }}
      </AutoSizer>
    </div>
  );
};

export default GifList;
