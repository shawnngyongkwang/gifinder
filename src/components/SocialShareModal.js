import React from "react";
import Modal from "react-modal";
import CopiedModal from "./CopiedModal";
import { IoMdCloseCircle } from "react-icons/io";
import {
  EmailShareButton,
  FacebookShareButton,
  RedditShareButton,
  TelegramShareButton,
  TumblrShareButton,
  TwitterShareButton,
  WhatsappShareButton,
  EmailIcon,
  FacebookIcon,
  RedditIcon,
  TelegramIcon,
  TumblrIcon,
  TwitterIcon,
  WhatsappIcon,
} from "react-share";
import clipboardIcon from "../assets/clipboard-icon.jpg";

class SocialShareModal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      copiedModalIsOpen: false,
    };
  }

  openCopiedModal = () => {
    this.setState(
      {
        copiedModalIsOpen: true,
      },
      () => {
        setTimeout(this.closeCopiedModal, 1500);
      }
    );
  };

  closeCopiedModal = () => {
    this.setState({
      copiedModalIsOpen: false,
    });
  };

  render() {
    return (
      <Modal
        className="social-share-modal"
        isOpen={this.props.socialShareModalIsOpen}
        onRequestClose={() => this.props.onRequestClose()}
        ariaHideApp={false}
      >
        <IoMdCloseCircle
          className="close-button"
          size={18}
          onClick={() => this.props.onRequestClose()}
        />
        <div className="social-share-container">
          <div className="social-share-row">
            <FacebookShareButton
              className="social-share-button"
              url={this.props.urlToShare}
            >
              <FacebookIcon className="social-share-icon" />
            </FacebookShareButton>
            <TwitterShareButton
              className="social-share-button"
              url={this.props.urlToShare}
            >
              <TwitterIcon className="social-share-icon" />
            </TwitterShareButton>
            <RedditShareButton
              className="social-share-button"
              url={this.props.urlToShare}
            >
              <RedditIcon className="social-share-icon" />
            </RedditShareButton>
          </div>
          <div className="social-share-row">
            <TumblrShareButton
              className="social-share-button"
              url={this.props.urlToShare}
            >
              <TumblrIcon className="social-share-icon" />
            </TumblrShareButton>
            <TelegramShareButton
              className="social-share-button"
              url={this.props.urlToShare}
            >
              <TelegramIcon className="social-share-icon" />
            </TelegramShareButton>
            <WhatsappShareButton
              className="social-share-button"
              url={this.props.urlToShare}
            >
              <WhatsappIcon className="social-share-icon" />
            </WhatsappShareButton>
          </div>
          <div>
            <EmailShareButton
              className="social-share-button"
              url={this.props.urlToShare}
            >
              <EmailIcon className="social-share-icon" />
            </EmailShareButton>
            <img
              className="social-share-button social-share-icon"
              src={clipboardIcon}
              alt="copy to clipboard"
              width="64px"
              height="auto"
              onClick={() => {
                navigator.clipboard.writeText(this.props.urlToShare);
                this.openCopiedModal();
              }}
              style={{ marginTop: "-48px" }}
            />
          </div>
          <CopiedModal copiedModalIsOpen={this.state.copiedModalIsOpen} />
        </div>
      </Modal>
    );
  }
}

export default SocialShareModal;
