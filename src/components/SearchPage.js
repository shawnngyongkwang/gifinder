import React, { useState, useRef, useCallback, useEffect } from "react";
import useGifSearch from "../useGifSearch";
import SearchBar from "./SearchBar";
import GifList from "./GifList";
import GifModal from "./GifModal";
import NavBar from "./NavBar";
import { Layout, Spin } from "antd";
import MediaQuery from "react-responsive";
import GIFinderLogo from "../assets/GIFinder-logo.png";

export default function SearchPage(props) {
  const [query, setQuery] = useState("");
  const [pageNumber, setPageNumber] = useState(1);
  const { loading, error, gifs, hasMore } = useGifSearch(query, pageNumber);
  const [selectedGif, setSelectedGif] = useState(null);
  const [modalIsOpen, setModalIsOpen] = useState(false);

  const authInstance = window.gapi.auth2.getAuthInstance();
  const user = authInstance.currentUser.get();
  const profile = user.getBasicProfile();
  const email = profile.getEmail();
  const { setFavourites } = props;

  const { Content, Footer } = Layout;

  const observer = useRef();
  const lastGifRef = useCallback(
    (node) => {
      if (loading) return;
      if (observer.current) observer.current.disconnect();
      observer.current = new IntersectionObserver((entries) => {
        if (entries[0].isIntersecting && hasMore) {
          setPageNumber((prevPageNumber) => prevPageNumber + 1);
        }
      });
      if (node) observer.current.observe(node);
    },
    [loading, hasMore]
  );

  const handleSearch = (query) => {
    setQuery(query);
    setPageNumber(1);
  };

  const clearTerm = () => {
    handleSearch("");
  };

  const openModal = (gif) => {
    setModalIsOpen(true);
    setSelectedGif(gif);
  };

  const closeModal = () => {
    setModalIsOpen(false);
    setSelectedGif(null);
  };

  useEffect(() => {
    const getFavourites = JSON.parse(localStorage.getItem(email) || "0");
    if (getFavourites !== 0) {
      setFavourites([...getFavourites]);
    } else {
      setFavourites([]);
    }
  }, [setFavourites, email]);

  return (
    <Layout className="layout">
      <MediaQuery maxWidth={550}>
        <div className="mobile-header-logo-container">
          <img
            className="mobile-header-logo"
            src={GIFinderLogo}
            alt="GIFinder logo"
          />
        </div>
      </MediaQuery>
      <div className="site-layout-header">
        <NavBar clearTerm={clearTerm} />
        <SearchBar onChange={handleSearch} query={query} />
      </div>
      <Content>
        <div className="site-layout-content">
          <GifList
            gifs={gifs}
            lastGifRef={lastGifRef}
            onGifSelect={(selectedGif) => openModal(selectedGif)}
          />
          <GifModal
            modalIsOpen={modalIsOpen}
            selectedGif={selectedGif}
            onRequestClose={() => closeModal()}
            favourites={props.favourites}
            setFavourites={props.setFavourites}
          />
        </div>
        <div style={{ backgroundColor: "white" }}>
          {loading && <Spin tip="Loading..." />}
        </div>
        <div>{error && "Error"}</div>
      </Content>
      <MediaQuery minHeight={700}>
        <Footer className="site-layout-footer">
          GIFinder, powered by GIPHY | Created by Shawn Ng | 2021
        </Footer>
      </MediaQuery>
    </Layout>
  );
}
