import React, { useState } from "react";
import Modal from "react-modal";
import SocialShareModal from "./SocialShareModal";
import { HiHeart, HiOutlineHeart } from "react-icons/hi";
import { IoMdCloseCircle } from "react-icons/io";
import moment from "moment";
import { Button } from "antd";
import MediaQuery from "react-responsive";

export default function GifModal(props) {
  const authInstance = window.gapi.auth2.getAuthInstance();
  const user = authInstance.currentUser.get();
  const profile = user.getBasicProfile();
  const email = profile.getEmail();

  const [socialShareModalIsOpen, setSocialShareModalIsOpen] = useState(false);

  const openSocialShareModal = () => {
    setSocialShareModalIsOpen(true);
  };

  const closeSocialShareModal = () => {
    setSocialShareModalIsOpen(false);
  };

  if (props.selectedGif === null) {
    return <div></div>;
  }

  const switchFav = () => {
    let addArray = true;
    props.favourites.map((item, key) => {
      if (item.url === props.selectedGif.url) {
        props.favourites.splice(key, 1);
        addArray = false;
      }
      return 0;
    });
    if (addArray) {
      props.favourites.push(props.selectedGif);
    }
    props.setFavourites([...props.favourites]);

    // Store in localStorage
    localStorage.setItem(email, JSON.stringify(props.favourites));

    var storage = localStorage.getItem("favItem" + props.selectedGif || "0");
    if (storage == null) {
      localStorage.setItem(
        "favItem" + props.selectedGif,
        JSON.stringify(props.favourites)
      );
    } else {
      localStorage.removeItem("favItem" + props.selectedGif);
    }
  };

  return (
    <Modal
      className="modal"
      isOpen={props.modalIsOpen}
      onRequestClose={() => props.onRequestClose()}
      ariaHideApp={false}
    >
      <IoMdCloseCircle
        className="close-button"
        size={18}
        onClick={() => props.onRequestClose()}
      />
      <img
        className="gif-modal-img"
        src={props.selectedGif.images.original.url}
        alt="selected gif"
      />
      <p className="gif-title">
        <strong>{props.selectedGif.title}</strong>
      </p>
      <MediaQuery minHeight={700}>
        {props.selectedGif.user && (
          <div className="gif-uploader">
            <img
              className="gif-uploader-img"
              src={props.selectedGif.user.avatar_url}
              alt="user avatar"
              height="auto"
            />
          </div>
        )}
        <p>
          <strong>Uploaded on: </strong>
          {moment(props.selectedGif.import_datetime).format("DD MMMM YYYY")}
        </p>
      </MediaQuery>
      <div className="gif-modal-button-container">
        <div className="love-button">
          {props.favourites
            .map((gif) => gif.url)
            .includes(props.selectedGif.url) ? (
            <HiHeart size={26} onClick={() => switchFav()} />
          ) : (
            <HiOutlineHeart size={26} onClick={() => switchFav()} />
          )}
        </div>
        <Button
          onClick={() => {
            if (navigator.share) {
              // if WebShare API is suported
              navigator
                .share({
                  title: `${props.selectedGif.title}`,
                  url: `${props.selectedGif.images.original.url}`,
                })
                .then(() => {})
                .catch(console.error);
            } else {
              // if WebShare API is not supported
              openSocialShareModal();
            }
          }}
        >
          share
        </Button>
      </div>
      <SocialShareModal
        socialShareModalIsOpen={socialShareModalIsOpen}
        onRequestClose={() => closeSocialShareModal()}
        urlToShare={props.selectedGif.images.original.url}
      />
    </Modal>
  );
}
