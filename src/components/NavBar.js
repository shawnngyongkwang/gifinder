import React from "react";
import { Link } from "react-router-dom";
import { Menu, Button } from "antd";
import GIFinderLogo from "../assets/GIFinder-logo.png";
import MediaQuery from "react-responsive";

export default function NavBar(props) {
  const authInstance = window.gapi.auth2.getAuthInstance();

  return (
    <div>
      <MediaQuery minWidth={550}>
        <Link
          to="/"
          onClick={() => {
            props.clearTerm();
          }}
        >
          <img className="logo" src={GIFinderLogo} alt="GIFinder logo" />
        </Link>
      </MediaQuery>
      <Menu
        className="menu"
        mode="horizontal"
        defaultSelectedKeys={[
          window.location.pathname === "/gifinder"
            ? "/gifinder/"
            : window.location.pathname,
        ]}
      >
        <Menu.Item key="/gifinder/" className="menu-item">
          <Link
            to="/"
            onClick={() => {
              props.clearTerm();
            }}
          >
            Home
          </Link>
        </Menu.Item>
        <Menu.Item key="/gifinder/favourites" className="menu-item">
          <Link
            to="/favourites"
            onClick={() => {
              props.clearTerm();
            }}
          >
            Favourites
          </Link>
        </Menu.Item>
      </Menu>
      <Link to="/" className="logout-button">
        <Button onClick={authInstance.signOut}>Sign out</Button>
      </Link>
    </div>
  );
}
