import React from "react";
import GIFinderLogo from "../assets/GIFinder-logo.png";
import { CSSTransition } from "react-transition-group";

class Login extends React.Component {
  componentDidMount() {
    window.gapi.load("signin2", () => {
      window.gapi.signin2.render("login-button");
    });
  }

  render() {
    return (
      <div className="login-container">
        <CSSTransition
          in={true}
          appear={true}
          timeout={1500}
          classNames="login-page-logo-container"
        >
          <div className="login-page-logo-container">
            <img
              src={GIFinderLogo}
              alt="GIFinder logo"
              className="login-page-logo"
            />
          </div>
        </CSSTransition>
        <div id="login-button">Sign in with Google</div>
      </div>
    );
  }
}

export default Login;
