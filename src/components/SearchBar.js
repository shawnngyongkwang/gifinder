import React from "react";
import { Input } from "antd";
import { SearchOutlined } from "@ant-design/icons";

class SearchBar extends React.Component {
  onInputChange(query) {
    this.props.onChange(query);
  }

  render() {
    return (
      <div className="search-bar">
        <Input
          prefix={<SearchOutlined />}
          placeholder="Let's search for GIFs!"
          allowClear
          value={this.props.query}
          onChange={(event) => this.onInputChange(event.target.value)}
          style={{ width: 200 }}
        />
      </div>
    );
  }
}

export default SearchBar;
