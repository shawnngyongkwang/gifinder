import React from "react";

const GifItem = (props) => {
  return (
    <li onClick={() => props.onGifSelect(props.gif)} className="gif-item">
      <img
        ref={props.lastGifRef}
        src={props.gif.images.original.url}
        alt="gif"
      />
    </li>
  );
};

export default GifItem;
