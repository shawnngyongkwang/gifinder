import React from "react";
import Modal from "react-modal";

export default function CopiedModal(props) {
  return (
    <Modal
      className="copied-modal"
      isOpen={props.copiedModalIsOpen}
      ariaHideApp={false}
    >
      <p>
        <strong>Copied to clipboard!</strong>
      </p>
    </Modal>
  );
}
