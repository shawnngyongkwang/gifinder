import React from "react";

const FavGifItem = (props) => {
  return (
    <li onClick={() => props.onGifSelect(props.gif)} className="gif-item">
      <img src={props.gif.images.downsized.url} alt="gif" />
    </li>
  );
};

export default FavGifItem;
