<p style="text-align: center">

   <a href="https://shawnngyongkwang.gitlab.io/gifinder">
    <img width='45%' src="./src/assets/GIFinder-logo.png">
  </a>

  <p style="text-align: center"><i>Infinite scrolling GIF finder</i></p>
</p>

# GIFinder

**GIFinder** is a GIF search web app powered by GIPHY. <br />
Search for thousands of GIFs, browse currently trending ones, save them to your favourites, and share them with friends with ease. <br />
[Let's get searching](https://shawnngyongkwang.gitlab.io/gifinder)!

## Main Technologies

|                               React                               |                               HTML                               |                              CSS                              |                         JavaScript                          |
| :---------------------------------------------------------------: | :--------------------------------------------------------------: | :-----------------------------------------------------------: | :---------------------------------------------------------: |
| <img width='32px' src="./assets/react-logo.png" alt="React logo"> | <img width='32px' src="./assets/html-logo.png" alt="HTML logo"/> | <img width='32px' src="./assets/css-logo.png" alt="CSS logo"> | <img width='32px' src="./assets/js-logo.png" alt="JS logo"> |

## Libraries Used

- [Ant Design](https://ant.design/)
- [React Google Login](https://www.npmjs.com/package/react-google-login)
- [React Icons](https://react-icons.github.io/react-icons/)
- [React Modal](https://www.npmjs.com/package/react-modal)
- [React Responsive](https://www.npmjs.com/package/react-responsive)
- [React Router](https://reactrouter.com/)
- [React Share](https://www.npmjs.com/package/react-share)
- [React Virtualized](https://github.com/bvaughn/react-virtualized)
- [React Transition Group](https://reactcommunity.org/react-transition-group/)
